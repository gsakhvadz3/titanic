import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    title_mapping = {"Mr.": "Mr", "Mrs.": "Mrs", "Miss.": "Miss"}
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False).map(title_mapping)

    filled_values = []
    for title in ['Mr', 'Mrs', 'Miss']:
        title_ages = df[df['Title'] == title]['Age']
        median_age = title_ages.median(skipna=True)
        missing_count = title_ages.isnull().sum()
        filled_values.append((title, missing_count, round(median_age)))

        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age

    return filled_values
